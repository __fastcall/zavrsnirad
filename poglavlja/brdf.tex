% Poglavlje "Funkcija distribucije dvosmjerne refleksije"
\chapter{Funkcija distribucije dvosmjerne refleksije}

% Potpoglavlje "Formalna definicija"
\section{Formalna definicija}

Funkcija distribucije dvosmjerne refleksije (engl. bidirectional reflectance distribution function, BRDF) je funkcija koja definira kako se svjetlost reflektira o neprozirnu površinu.
Često se koristi u optičkim proračunima u stvarnom svijetu, te u algoritmima računalne grafike i računalnog vida.
Funkcija ovisi o smjeru svjetlosti pri ulasku \(\omega_{i}\), smjeru svjetlosti na izlasku \(\omega_{o}\), položaju točke u prostoru \(\mathbf{x}\), boji ili valnoj duljini svjetlosti \(\lambda\) i trenutku u vremenu \(t\).
Na slici \ref{fig:brdf-syms} je prikazana ilustracija koja predstavlja parametre funkcije prikazane u perspektivi prostora.

\bigskip
\bigskip

% Slika parametara funkcije distribucije dvosmjerne refleksije
\begin{figure}[htb]
\centering
\includegraphics[width=8.5cm]{figure/brdf_syms.png}
\caption{Parametri funkcije distribucije dvosmjerne refleksije}
\label{fig:brdf-syms}
\end{figure}

\clearpage

% Potpoglavlje "Cook-Torranceov model"
\section{Cook-Torranceov model}

Cook-Torranceov model je jedan od najčešćih modela korištenih za izračun funkcije distribucije dvosmjerne refleksije.
Ponašanje svjetlosti u modelu razlikuje difuznu i spekularnu komponentu.
Na slici \ref{fig:cook-torrance-brdf} je prikazan izraz za funkciju distribucije dvosmjerne refleksije.
Izraz \(f_{lambert}\) opisuje difuznu komponentu, izraz \(f_{cook-torrance}\) opisuje spekularnu komponentu, a izrazi \(k_{d}\) i \(k_{s}\) opisuju udio svake od komponenti.

\bigskip

% Slika funkcije distribucije dvosmjerne refleksije Cook-Torranceovog modela
\begin{figure}[htb]
\[  f_{r} = k_{d} f_{lambert} + k_{s} f_{cook-torrance} \]
\caption{Funkcija distribucije dvosmjerne refleksije Cook-Torranceovog modela [\citenum{codinglabs}]}
\label{fig:cook-torrance-brdf}
\end{figure}

\bigskip

Difuznu komponentu postavljamo na konstantu kao što se može uočiti na slici \ref{fig:lambertian-diffuse}.
Izraz \(c\) predstavlja albedo boju materijala.
Boja je podijeljena sa \(\pi\) jer, ako se prisjetimo, čitav izraz za \(f_{r}\) se nalazi pod integralom koji će nakon integracije čitav rezultat skalirati sa \(\pi\).
Također, difuznu komponentu nije potrebno množiti s \((\omega_{i} \cdot \mathbf{n})\) s obzirom da se taj izraz već nalazi unutar integrala.

\bigskip

% Slika izraza Lambertove difuzne komponente
\begin{figure}[htb]
\[  f_{lambert} = \frac{c}{\pi} \]
\caption{Izraz Lambertove difuzne komponente [\citenum{codinglabs}]}
\label{fig:lambertian-diffuse}
\end{figure}

\bigskip

Izraz za spekularnu komponentu je nešto kompliciraniji kao što vidimo na slici \ref{fig:cook-torrance-specular}.
U izrazu \(D\) predstavlja funkciju distribucije normala, \(F\) predstavlja Fresnelov koeficijent, a \(G\) predstavlja geometrijsku funkciju.

\bigskip

% Slika izraza Cook-Torranceove spekularne komponente
\begin{figure}[htb]
\[  f_{cook-torrance} = \frac{D F G}{4 (\omega_{o} \cdot \mathbf{n}) (\omega_{i} \cdot \mathbf{n})} \]
\caption{Izraz Cook-Torranceove spekularne komponente [\citenum{codinglabs}]}
\label{fig:cook-torrance-specular}
\end{figure}

\clearpage

% Potpoglavlje "Funkcija distribucije normala"
\section{Funkcija distribucije normala}

Funkcija distribucije normala \(D\) (engl. normal distribution function) statistički aproksimira omjer mikrozrcala koji su poravnati s polovičnim vektorom \(\hat{h}\).
Parametri funkcije su normala površine \(\mathbf{n}\), polovični vektor \(\hat{h}\) te faktor \(\alpha\).
Faktor \(\alpha\) je mjera koja ovisi o faktoru gruboće, a uobičajeno je jednaka njegovom kvadratu.

% Slika funkcije distribucije normala GGX Trowbridge-Reitz
\begin{figure}[htb]
\[  D_{GGXTR} (\mathbf{n}, \hat{h}, \alpha) = \frac{\alpha^{2}}{\pi ((\mathbf{n} \cdot \hat{h})^{2} (\alpha^{2} - 1) + 1)^{2}} \]
\caption{Funkcija distribucije normala GGX Trowbridge-Reitz [\citenum{graphicrants}]}
\label{fig:ndf-ggx-tr}
\end{figure}

Postoje mnoge funkcije koje aproksimiraju funkciju distribucije normala.
Jedna od njih je funkcija distribucije normala GGX Trowbridge-Reitz korištena u implementaciji, prikazana na slici \ref{fig:ndf-ggx-tr}.
Bitno je uočiti da je rezultat funkcija distribucije normala skalar.

% Slika funkcije distribucije normala na kugli po različitim faktorima gruboće
\begin{figure}[htb]
\centering
\includegraphics[width=14cm]{figure/roughness.png}
\caption{Funkcija distribucije normala na kugli po različitim faktorima gruboće}
\label{fig:ndf-ggx-tr-spheres}
\end{figure}

Na slici \ref{fig:ndf-ggx-tr-spheres} možemo uočiti vrijednosti funkcije distribucije normala u raznim točkama kugle za različite vrijednosti faktora gruboće.
Za glatke plohe je velik omjer poravnatih mikrozrcala koncentriran na maloj površini dok je za grube plohe raširen po većem dijelu površine u manjim omjerima.

\bigskip

% Potpoglavlje "Fresnelov koeficijent"
\section{Fresnelov koeficijent}

Fresnelov koeficijent \(F\) (engl. Fresnel coefficient) opisuje omjer svjetlosti koja se reflektira naspram svjetlosti koja se refraktira.
On ovisi o polovičnom vektoru \(\hat{h}\) i kutu kojim se gleda na površinu \(\hat{v}\).
Na slici \ref{fig:fresnel-schlick} je prikazana Fresnel-Schlickova funkcija, jedna od funkcija kojom se može aproksimirati Fresnelov koeficijent.

\clearpage

% Slika Fresnel-Schlickove funkcije
\begin{figure}[htb]
\[ F_{Schlick}(\hat{h}, \hat{v}, F_{0}) = F_{0} + (1 - F_{0}) (1 - (\hat{h} \cdot \hat{v}))^{5} \]
\caption{Fresnel-Schlickova funkcija [\citenum{graphicrants}]}
\label{fig:fresnel-schlick}
\end{figure}

Bazna reflektivnost površine je označena sa \(F_{0}\).
To je boja koja za metalne površine poprima visoke vrijednosti dok za dielektrike poprima niske vrijednosti.
Prikazane u tablici \ref{tbl:common-f0-values} su vrijednosti bazne reflektivnosti za neke površine.

\bigskip

\begin{table}[htb]
\caption{Vrijednosti bazne reflektivnosti za neke površine [\citenum{natyhoffmannotes}]}
\label{tbl:common-f0-values}
\centering
\begin{tabular}{lllc} \hline
Materijal & \(F_{0}\) (Linearna) & \(F_{0}\) (sRGB) & Boja\\ \hline
Voda & \((0.02, 0.02, 0.02)\) & \((0.15, 0.15, 0.15)\) & \scalebox{1.25}{\(\textcolor[rgb]{0.15, 0.15, 0.15}{\blacksquare}\)} \\
Plastika / staklo (niska) & \((0.03, 0.03, 0.03)\) & \((0.21, 0.21, 0.21)\) & \scalebox{1.25}{\(\textcolor[rgb]{0.21, 0.21, 0.21}{\blacksquare}\)} \\
Plastika (visoka) & \((0.05, 0.05, 0.05)\) & \((0.24, 0.24, 0.24)\) & \scalebox{1.25}{\(\textcolor[rgb]{0.24, 0.24, 0.24}{\blacksquare}\)} \\
Staklo (visoka) / rubin & \((0.08, 0.08, 0.08)\) & \((0.31, 0.31, 0.31)\) & \scalebox{1.25}{\(\textcolor[rgb]{0.31, 0.31, 0.31}{\blacksquare}\)} \\
Dijamant & \((0.17, 0.17, 0.17)\) & \((0.45, 0.45, 0.45)\) & \scalebox{1.25}{\(\textcolor[rgb]{0.45, 0.45, 0.45}{\blacksquare}\)} \\
Željezo & \((0.56, 0.57, 0.58)\) & \((0.77, 0.78, 0.78)\) & \scalebox{1.25}{\(\textcolor[rgb]{0.77, 0.78, 0.78}{\blacksquare}\)} \\
Bakar & \((0.95, 0.64, 0.54)\) & \((0.98, 0.82, 0.76)\) & \scalebox{1.25}{\(\textcolor[rgb]{0.98, 0.82, 0.76}{\blacksquare}\)} \\
Zlato & \((1.00, 0.71, 0.29)\) & \((1.00, 0.86, 0.57)\) & \scalebox{1.25}{\(\textcolor[rgb]{1.00, 0.86, 0.57}{\blacksquare}\)} \\
Aluminij & \((0.91, 0.92, 0.92)\) & \((0.96, 0.96, 0.97)\) & \scalebox{1.25}{\(\textcolor[rgb]{0.96, 0.96, 0.97}{\blacksquare}\)} \\
Srebro & \((0.95, 0.93, 0.88)\) & \((0.98, 0.97, 0.95)\) & \scalebox{1.25}{\(\textcolor[rgb]{0.98, 0.97, 0.95}{\blacksquare}\)} \\ \hline
\end{tabular}
\end{table}

\bigskip

Također je moguće napisati i oblik funkcije koji ovisi o normali površine \(\mathbf{n}\), vektoru pogleda \(\hat{v}\), baznoj reflektivnosti \(F_{0}\) te također i faktoru gruboće, ovdje označenog kao \(r\) na slici \ref{fig:fresnel-schlick-roughness}.

\bigskip

% Slika Fresnel-Schlickove funkcije s faktorom gruboće
\begin{figure}[htb]
\[ F_{Schlick-Roughness}(\mathbf{n}, \hat{v}, F_{0}, r) = F_{0} + ( \max((1 - r, 1 - r, 1 - r), F_{0}) - F_{0}) (1 - (\mathbf{n} \cdot \hat{v}))^{5} \]
\caption{Fresnel-Schlickova funkcija s faktorom gruboće [\citenum{graphicrants}]}
\label{fig:fresnel-schlick-roughness}
\end{figure}

Na slici \ref{fig:fresnel-schlick-distribution} možemo vidjeti primjenu navedene funkcije na kugli za različite vrijednosti faktora gruboće.

\clearpage

% Slika Fresnel-Schlickove funkcija s faktorom gruboće po različitim faktorima gruboće
\begin{figure}[htb]
\centering
\includegraphics[width=14cm]{figure/fresnel.png}
\caption{Fresnel-Schlickova funkcija s faktorom gruboće po različitim faktorima gruboće}
\label{fig:fresnel-schlick-distribution}
\end{figure}

% Potpoglavlje "Geometrijska funkcija"
\section{Geometrijska funkcija}

Geometrijska funkcija \(G\) (engl. geometry function) je funkcija koja statistički aproksimira relativnu površinu gdje se njezini detalji na mikroskopskoj razini međusobno zasjenjuju uzrokovajući upijanje zraka svjetlosti.
Funkcija ovisi o normali površine \(\mathbf{n}\), vektoru smjera pogleda \(\hat{v}\), vektoru smjera izvora svjetlosti \(\hat{l}\) te remapiranom koeficijentu gruboće \(k\).
Na slici \ref{fig:geometry-function-schlick-ggx} je prikazana Schlick-Beckmannova GGX aproksimacija geometrijske funkcije.

% Slika geometrijske funkcija Schlick-Beckmann GGX
\begin{figure}[htb]
\[ G_{SchlickGGX}(\mathbf{n}, \hat{v}, k) = \frac{\mathbf{n} \cdot \hat{v}}{(\mathbf{n} \cdot \hat{v})(1 - k) + k} \]
\caption{Geometrijska funkcija Schlick-Beckmann GGX [\citenum{graphicrants}]}
\label{fig:geometry-function-schlick-ggx}
\end{figure}

Koeficijent \(k\) se može izračunati na različite načine.
Na slici \ref{fig:geometry-function-k} su prikazana dva načina izračuna.
Na lijevoj strani je prikazan izraz za izravan izračun, a na desnoj je prikazan izraz koji se koristi u kombinaciji s tehnikom osvjetljavanja temeljenog na slici.

% Slika konstanti Schlick-Beckmann GGX geometrijske funkcije
\begin{figure}[htb]
\[ k_{direct} = \frac{(\alpha + 1)^{2}}{8}  \:\:\:\:\:\:\:\:\:\:\:\:\:\:\: k_{IBL} = \frac{\alpha^{2}}{2} \]
\caption{Konstante Schlick-Beckmann GGX geometrijske funkcije [\citenum{graphicrants}]}
\label{fig:geometry-function-k}
\end{figure}

\bigskip

Za efektivniju aproksimaciju geometrijske funkcije, potrebno je još uključiti i vektor izvora svjetlosti \(\hat{l}\).
To možemo izvesti koristeći Smithovu metodu aproksimacije prikazanu na slici \ref{fig:geometry-function-smith}.

\clearpage

% Slika Smithove metoda aproksimacije geometrijske funkcije
\begin{figure}[htb]
\[ G_{Smith}(\mathbf{n}, \hat{v}, \hat{l}, k) = G_{SchlickGGX}(\mathbf{n}, \hat{v}, k) G_{SchlickGGX}(\mathbf{n}, \hat{l}, k) \]
\caption{Smithova metoda aproksimacije geometrijske funkcije [\citenum{graphicrants}]}
\label{fig:geometry-function-smith}
\end{figure}

Na slici \ref{fig:geometry-comparison} je prikazana usporedba Smithove aproksimacije geometrijske funkcije na kugli po različitim faktorima gruboće.

% Slika Smithove geometrijska funkcija po različitim faktorima gruboće
\begin{figure}[htb]
\centering
\includegraphics[width=14cm]{figure/geometry-sphere.png}
\caption{Smithova geometrijska funkcija po različitim faktorima gruboće}
\label{fig:geometry-comparison}
\end{figure}
