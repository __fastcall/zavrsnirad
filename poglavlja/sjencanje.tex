% Poglavlje "Sjenčanje".
\chapter{Sjenčanje}

% Potpoglavlje "Sjenčari"
\section{Sjenčari}

Sjenčar (engl. shader) je općeniti naziv za program koji se izvršava na grafičkoj kartici.
Prva moderna upotreba riječi se pojavila unutar specifikacije sučelja alata RenderMan animacijskog studija Pixar.

GLSL (engl. GL Shading Language) je programski jezik koji se koristi za pisanje programa sjenčara OpenGL aplikacijskog sučelja.
Po sintaksi je sličan jezicima C i C++.
Upravljačkom programu (engl. driver) se izvorni kod sjenčara predaje u tekstualnom obliku koji se onda prevodi u strojni jezik specifičan prisutnom sklopovlju.
Također je u novijim verzijama OpenGL-a moguće predati već preveden kod sjenčara u posredničkom strojnom jeziku SPIR-V (engl. Standard, Portable Intermediate Representation - V).
Posebnost strojnog jezika SPIR-V je da, osim što se GLSL može prevesti u njega, također postoje i prevoditelji Microsoftovog jezika HLSL (engl. High-Level Shading Language) korištenog unutar Direct3D aplikacijskog sučelja.

% Slika grafičkog protočnog sustava
\begin{figure}[htb]
\centering
\includegraphics[width=3.3cm]{figure/pipeline.png}
\caption{Grafički protočni sustav [\citenum{khronosdoc}]}
\label{fig:pipeline}
\end{figure}

Na slici \ref{fig:pipeline} je prikazan grafički protočni sustav koji je iniciran izdavanjem naredbe za crtanje objekta.
U prvom koraku se dohvaća tok vrhova iz objekta niza vrhova (engl. Vertex Array Object) te se šalje u sjenčar vrhova koji nad njima obavlja željene operacije.
Ako su teselacijski sjenčari zadani, transformirani vrhovi ulaze u teselacijsku obradu.
Isto vrijedi i za geometrijski sjenčar.
U slijedećim koracima se odvija odsijecanje prema volumenu pogleda, odsijecanje prema korisnikovim specifikacijama (ako ih je zadao u sjenčaru), dijeljenje perspektive te transformacija u prostor pogleda (engl. viewport).
Konačno, pred kraj se odvija rasterizacija vrhova u fragmente, te se za svaki zasebni fragment aktivira sjenčar fragmenata.
Na izlazu iz sjenčara fragmenata se provodi niz ispitivanja nad fragmentima ako su ona omogućena u OpenGL kontekstu.

% Potpoglavlje "Sjenčar vrhova"
\section{Sjenčar vrhova}

Sjenčar vrhova (engl. vertex shader) je dio programa sjenčanja koji obavlja transformacije nad vrhovima.
Tok atributa vrhova je učitan iz međuspremnika postavljenog objekta niza vrhova OpenGL konteksta.

% Slika primjera raspona atributa vrhova
\begin{figure}[htb]
\centering
\includegraphics[width=11cm]{figure/stride.png}
\caption{Primjer raspona atributa vrhova}
\label{fig:stride}
\end{figure}

Na slici \ref{fig:stride} je prikazan primjer raspona atributa vrhova (engl. stride).
Sastoji se od osam vrijednosti s pomičnim zarezom jednostruke preciznosti, od kojih prve tri čine lokalnu poziciju vrha, sljedeće dvije čine UV teksturne koordinate, a zadnje tri čine lokalnu normalu.
Upravo ova raspodjela je korištena u implementaciji, no naravno po potrebi je moguće dodati još atributa kao što su primjerice tangenta, bitangenta, stvarna normala itd.

Za omogućavanje atributa se poziva funkcija \texttt{glEnableVertexAttribArray} s \, parametrom \, indeksa \, atributa.
\, Mapiranje \, atributa \, se \, ostvaruje \, pozivom \, funkcije \texttt{glVertexAttribPointer}, kojoj se kao parametar predaje indeks atributa, tip te duljina podataka, ukupna duljina raspona i odmak od početka raspona.
Po potrebi se može omogućiti normalizacija atributa.

% Slika primjera jednostavnog sjenčara vrhova u jeziku GLSL
\begin{figure}[htb]
\lstset{language=GLSL, tabsize=2}
\begin{lstlisting}[basicstyle=\small]
#version 410 core
layout (location = 0) in vec3 local_pos;
layout (location = 1) in vec2 local_uv;
layout (location = 2) in vec3 local_norm;
uniform mat4 proj_view;
uniform mat4 model;
out vec3 world_pos;
out vec2 uv;
out vec3 world_norm;
void main() {
    world_pos = (model * vec4(local_pos, 1.0)).xyz;
    uv = local_uv;
    world_norm = normalize(mat3(model) * local_norm);
    gl_Position = proj_view * vec4(world_pos, 1.0);
}
\end{lstlisting}
\caption{Primjer jednostavnog sjenčara vrhova u jeziku GLSL}
\label{fig:vertex-shader}
\end{figure}

Na slici \ref{fig:vertex-shader} je prikazan primjer relativno jednostavnog sjenčara vrhova napisanog u jeziku za sjenčanje GLSL.
Direktivom \texttt{\#version} je deklarirana željena verzija jezika za sjenčanje.
Tipovi deklarirani s prefiksom \texttt{in} definiraju ulazne tipove podataka u sjenčar, u ovom slučaju atribute vrhova locirane na indeksu parametra \texttt{location}.
Tipovi deklarirani s prefiksom \texttt{uniform} predstavljaju vrijednosti zadane sjenčaru iz vanjskog programa.
Oni su prisutni unutar svih stadija sjenčanja, a vrijednost im je jednaka za sve vrhove.
Konačno, tipovi deklarirani s prefiksom \texttt{out} definiraju izlazne tipove podataka koji se propagiraju u sljedeći stadij grafičkog protočnog sustava.

Unutar ulazne funkcije \texttt{main} provodimo transformaciju lokalne pozicije vrha u poziciju unutar svijeta.
Koordinate teksture se samo propagiraju u sljedeći stadij s obzirom da za uzorkovanje nije potrebno provesti nikakvu transformaciju.
Na lokalnu normalu primjenjujemo transformacijsku matricu \texttt{model} skraćenu na tri dimenzije konstruktorom \texttt{mat3}.
Time izbacujemo translaciju ostavljajući preostale transformacije - pretpostavka je da će skaliranje biti uniformno i da neće utjecati na smjer normale.
Također ju još i normaliziramo tako da dobijemo točniji rezultat nakon što se primjeni interpolacija nad njom.
U implicitnu izlaznu varijablu \texttt{gl\_Position} stavljamo poziciju transformiranog vrha u sustavu promatrača (dakle, nakon primjene matrice pogleda i projekcije).
Varijabla je u homogenom prostoru, te sadrži informaciju o 2D poziciji na ekranu i dubini vrha.
Ta se varijabla interpolira i za svaki fragment će biti aktiviran sjenčar fragmenata s također interpoliranim ostalim vrijednostima izlaza.

% Potpoglavlje "Teselacijski sjenčari"
\section{Teselacijski sjenčari}

Teselacija ili popločavanje je postupak u kojemu se primitiv rastavlja na veći broj manjih primitiva.
Postupak je noviji dodatak grafičkom protočnom sustavu, a njegova prisutnost se može provjeriti ispitivanjem \texttt{GL\_ARB\_tessellation\_shader} [\citenum{khronosdoc}].
Ovaj sjenčar nije korišten u implementaciji, no veoma je koristan za dodavanje detalja objektima koji se nalaze bliže kameri tj. oku uzorkovanjem visinske mape (engl. height map).

Postoje dva tipa teselacijskih sjenčara: kontrolni teselacijski sjenčar (engl. tessellation control shader) i evaluacijski teselacijski sjenčar (engl. tessellation evaluation shader).
Kontrolni se koristi za zadavanje parametara i uvjeta raspodjele vrhova primitiva, dok evaluacijski se koristi za izračun konačne pozicije novodobivenih vrhova.
Primjena teselacije je prikazana na slici \ref{fig:tessellation}.

% Slika primjene teselacije
\begin{figure}[htb]
\centering
\includegraphics[width=14cm]{figure/tessellation.png}
\caption{Primjena teselacije}
\label{fig:tessellation}
\end{figure}

% Potpoglavlje "Geometrijski sjenčar"
\section{Geometrijski sjenčar}

Geometrijski sjenčar (engl. geometry shader) je tip sjenčara koji upravlja procesuiranjem primitiva.
Kao i teselacijski sjenčari, geometrijski sjenčar je noviji dodatak grafičkom protočnom sustavu čija se prisutnost može provjeriti ispitivanjem prisutnosti proširenja \texttt{GL\_ARB\_geometry\_shader4} [\citenum{khronosdoc}].
Također ni ovaj sjenčar nije korišten u implementaciji, no veoma je koristan pri prikazivanju čestica ili većeg broja istih primitiva čijom je pozicijom lakše upravljati samom transformacijom nad jednim vrhom nego nad čitavim primitivom.

Geometrijski sjenčar može pretvarati primitive iz jednog tipa u drugi, a najčešće je korišten pri pretvorbi vrhova u trokute ili četverokute.
Na slici \ref{fig:geometry} je prikazana primjena geometrijskog sjenčara.

% Slika primjene geometrijskog sjenčara
\begin{figure}[htb]
\centering
\includegraphics[width=14cm]{figure/geometry.png}
\caption{Primjena geometrijskog sjenčara}
\label{fig:geometry}
\end{figure}

\bigskip
\bigskip

% Potpoglavlje "Sjenčar fragmenata"
\section{Sjenčar fragmenata}

Sjenčar fragmenata (engl. fragment shader) je tip sjenčara koji obrađuje svaki individualni fragment generiran rasterizacijskim stadijem grafičkog protočnog sustava.
Ovaj sjenčar je konačni korak u kojemu se određuje boja, prozirnost i dubina fragmenta.
Kako će se fragment ugraditi u prikazanu sliku određuju postavke konteksta vezane uz ispitivanje dubine (engl. depth testing), miješanje boja (engl. blending), odsijecanje (engl. scissor testing) i sl.

Na slici \ref{fig:fragment-shader} je prikazan primjer jednostavnog sjenčara fragmenata napisanog u jeziku GLSL.
Unutar ulazne funkcije \texttt{main} je implementiran naivni model izračuna intenziteta boje.
Vektor od pozicije fragmenta u svjetskom prostoru do pozicije točkastog izvora svjetla je izračunat i pretvoren u jedinični vektor.
Intenzitet svjetla je onda izračunat kao kosinus kuta (to jest skalarni produkt) između tog vektora i vektora normale fragmenata, te je zaokružen na vrijednost veću ili jednaku 0.1.
Boja se onda određuje uzorkovanjem difuzne teksture i množenjem s prethodno izračunatim intenzitetom.
Varijabla \texttt{color} je izlazna varijabla koja opisuje vrijednosti RGBA komponenti boje fragmenata.

\clearpage

% Slika primjera jednostavnog sjenčara fragmenata u jeziku GLSL
\begin{figure}[htb]
\lstset{language=GLSL, tabsize=2}
\begin{lstlisting}[basicstyle=\small]
#version 410 core
in vec3 world_pos;
in vec2 uv;
in vec3 world_norm;
uniform vec3 light_pos;
uniform sampler2D diffuse;
out vec4 color;
void main() {
    vec3 to_light = normalize(light_pos - world_pos);
    float light_int =
        max(0.1, dot(to_light, normalize(world_norm)));
    color = vec4(light_int * texture(diffuse, uv).rgb, 1.0);
}
\end{lstlisting}
\caption{Primjer jednostavnog sjenčara fragmenata u jeziku GLSL}
\label{fig:fragment-shader}
\end{figure}

Ovaj pristup je jednostavna demonstracija sjenčara fragmenata i služi samo kao uvodni korak.
U sljedećim poglavljima će biti opisane neke od jednadžbi fizikalno temeljenog modela prikazivanja koje se mogu koristiti umjesto ove za dobivanje uvjerljivije i realističnije slike.

% Potpoglavlje "Računski sjenčar"
\section{Računski sjenčar}

Računski sjenčar (engl. compute shader) je tip sjenčara koji nije vezan za grafički protočni sustav.
Može biti korišten za generiranje prikaza, no njegova češća uporaba je za zadaće koje nisu izravno povezane s prikazivanjem poligona.
Po ideji je srodan aplikacijskom programskom sučelju OpenCL.
Prisutnost računskog sjenčara se može provjeriti ispitivanjem \texttt{GL\_ARB\_compute\_shader} [\citenum{khronosdoc}] proširenja.
