% Poglavlje "Interakcija svjetlosti s različitim površinama".
\chapter{Interakcija svjetlosti s različitim površinama}

% Potpoglavlje "Model mikrozrcala"
\section{Model mikrozrcala}

Svi fizikalno temeljeni modeli ostvarivanja prikaza se baziraju na teoriji modela mikrozrcala (engl. microfacets model theory).
Teorija opisuje da se bilo koja površina na mikroskopskoj razini može opisati kao nakupina malih savršeno reflektivnih zrcala nazvanih mikrozrcala (engl. microfacets).
Što je grublja površina, to su mikrozrcala kaotičnije poravnata u odnosu na ravninu.
Učinak toga jest da za grublju površinu reflektirana svjetlost će biti rasprostranjena raspršeno, dok u kontrastu za glatku površinu će reflektirana svjetlost biti rasprostranjena otprilike u istom smjeru.

% Slika reflektiranih zraka svjetlosti modela mikrozrcala
\begin{figure}[htb]
\centering
\includegraphics[width=14cm]{figure/microfacets.png}
\caption{Reflektirane zrake svjetlosti modela mikrozrcala}
\label{fig:microfacets}
\end{figure}

Na slici \ref{fig:microfacets} je s lijeve strane prikazana gruba površina, a s desne glatka površina.
Žutom bojom su nacrtane upadne zrake iz izvora svjetlosti, dok su s plavom bojom nacrtane reflektirane zrake.
Sa slike se može zaključiti da će za grube površine spekularna refleksija biti slabija ali raširenija, dok će za glatke površine biti oštrija ali sažetija.

Uvedimo sad izraz polovičnog vektora \(\hat{h}\) (engl. halfway vector) kao vektor koji se nalazi ‘‘na pola puta’’ između vektora smjera izvora svjetla \(\hat{l}\) i vektora smjera pogleda \(\hat{v}\).
Izraz za polovični vektor je dan na slici \ref{fig:halfway-vector}.

% Slika izraza za polovični vektor
\begin{figure}[htb]
\[\hat{h} = \frac{\hat{l} + \hat{v}}{\lvert\lvert\hat{l} + \hat{v}\lvert\lvert}\]
\caption{Izraz za polovični vektor}
\label{fig:halfway-vector}
\end{figure}

Nadalje možemo definirati faktor gruboće (engl. roughness) kao vrijednost iz domene \([0, 1]\).
Određen tim faktorom će biti udio mikrozrcala čija je normala poravnata s polovičnim vektorom \(\hat{h}\).

% Slika intenziteta spekularne refleksije kugle po različitim faktorima gruboće
\begin{figure}[htb]
\centering
\includegraphics[width=14cm]{figure/roughness.png}
\caption{Intenzitet spekularne refleksije kugle po različitim faktorima gruboće}
\label{fig:specular-reflection-roughness}
\end{figure}

Na slici \ref{fig:specular-reflection-roughness} je na kugli prikazan udio mikrozrcala poravnatih s polovičnim vektorom \(\hat{h}\) po različitim faktorima gruboće.
Bijela boja odgovara većem udjelu, a crna manjem.

% Potpoglavlje "Očuvanje energije"
\section{Očuvanje energije}

Aproksimacija modela mikrozrcala uključuje i pravilo očuvanja energije, to jest, energija izlazne svjetlosti ne smije biti veća od energije ulazne svjetlosti (osim kod emisivnih površina).
Kad se svjetlost sudari s površinom dijeli se na dio koji je reflektiran i dio koji je refraktiran.
Nakon dodatnih sudara refraktirane svjetlosti unutar površine tijela može doći do naknadnog izviranja dijela iste zrake koja će kontribuirati difuznoj boji površine.
Taj efekt se naziva potpovršinsko raspršivanje (engl. subsurface scattering) i sjenčari koji to uzimaju u obzir realističnije prikazuju materijale kao što su ljudska koža, mramor i vosak.
Jednostavniji modeli zanemaruju taj učinak i pretpostavljaju da će čitava refraktirana svjetlost biti apsorbirana u površini tijela.
Nadalje, metalne površine slijede ista pravila refleksije i refrakcije, no za razliku od dielektrika (nemetalnih materijala) u potpunosti apsorbiraju čitavu refraktiranu svjetlost bez raspršivanja, što znači da nemaju difuznu boju.
Upravo zbog toga se uvodi faktor metalnosti (engl. metalness) koji nam omogućuje da tretiramo različito metalne površine i dielektrike.
Raspon faktora metalnosti je \([0, 1]\), no uobičajeno je da za većinu materijala postavljamo vrijednost na 1 (u potpunosti metalna površina) ili na 0 (u potpunosti nemetalna površina).

Sljedeća važna observacija jest da su reflektirana i refraktirana svjetlost međusobno ekskluzivne.
To znači da za udio difuzne svjetlosti \(k_{d}\) i udio spekularne svjetlosti \(k_{s}\) vrijedi jednadžba na slici \ref{fig:energy-conservation}.

\bigskip

% Slika izraza za očuvanje energije svjetlosti
\begin{figure}[htb]
\[k_{d} + k_{s} = 1\]
\caption{Izraz za očuvanje energije svjetlosti}
\label{fig:energy-conservation}
\end{figure}

\bigskip
\bigskip

% Potpoglavlje "Radiometrijske mjere"
\section{Radiometrijske mjere}

Prostorni kut (engl. solid angle) je mjera površine oblika projiciranog na kuglu radijusa 1. Oznaka za mjernu jedinicu je \(\si{sr}\) (steradijan).
Energija isijavanja (engl. radiant energy) je radiometrijska mjera elektromagnetske radijacije. Korištena mjerna jedinica je \(\si{J}\) (Joule).
Tok isijavanja (engl. radiant flux) je radiometrijska mjera za energiju isijavanja emitiranu, reflektiranu, transmitiranu ili primljenu po jedinici vremena. Korištena mjerna jedinica je \(\si{W}\) (Watt).
Sjajnost (engl. radiance) je radiometrijska mjera za tok isijavanja emitiran, reflektiran, transmitiran ili primljen po jedinici steradijana i jedinici projicirane površine.

\clearpage

% Potpoglavlje "Jednadžba ostvarivanja prikaza i jednadžba reflektivnosti"
\section[Jednadžba ostvarivanja prikaza i jednadžba reflektivnosti]{Jednadžba ostvarivanja prikaza i jednadžba\\reflektivnosti}

Jednadžba ostvarivanja prikaza (engl. rendering equation) je integralna jednadžba u kojoj se prikazuje izlazna sjajnost točke kao suma emitirane i reflektirane sjajnosti.

% Slika jednadžbe ostvarivanja prikaza
\begin{figure}[htb]
\[  L_{o}(\mathbf{x}, \omega_{o}, \lambda, t) = L_{e}(\mathbf{x}, \omega_{o}, \lambda, t) + \int_{\Omega}{ f_{r}(\mathbf{x}, \omega_{i}, \omega_{o}, \lambda, t) L_{i}(\mathbf{x}, \omega_{i}, \lambda, t)(\omega_{i} \cdot \mathbf{n}) } \mathrm{d}\omega_{i} \]
\caption{Jednadžba ostvarivanja prikaza}
\label{fig:rendering-equation}
\end{figure}

Na slici \ref{fig:rendering-equation} je prikazana jednadžba ostvarivanja prikaza.
Značenje svih oznaka je objašnjeno u nastavku:
\begin{itemize}
\item \(L_{o}\) je ukupna sjajnost valne duljine \(\lambda\) usmjerena izvan prema \(\omega_{o}\) smjeru u trenutku \(t\) iz točke \(\mathbf{x}\).
\item \(\mathbf{x}\) je točka u prostoru.
\item \(\omega_{o}\) je smjer svjetlosti na izlasku.
\item \(\lambda\) je valna duljina svjetlosti.
\item \(t\) je trenutak u vremenu.
\item \(L_{e}\) je emitirana sjajnost.
\item \(\Omega\) je jedinična polukugla centrirana oko normale \(\mathbf{n}\) koja sadrži sve moguće vrijednosti za \(\omega_{i}\).
\item \(f_{r}\) je funkcija distribucije dvosmjerne refleksije - omjer svjetlosti reflektiran od \(\omega_{i}\) do \(\omega_{o}\) u točki \(\mathbf{x}\), vremenu \(t\) i pri valnoj duljini \(\lambda\).
\item \(\omega_{i}\) je negiran smjer svjetlosti na ulasku.
\item \(L_{i}\) je sjajnost valne duljine \(\lambda\) nadolazeće prema točki \(\mathbf{x}\) iz smjera \(\omega_{i}\) u vremenu \(t\).
\item \(\mathbf{n}\) je površinska normala u točki \(\mathbf{x}\).
\item \((\omega_{i} \cdot \mathbf{n})\) je faktor oslabljivanja, često zapisan kao \(\cos{\theta_{i}}\) gdje je \(\theta_{i}\) kut između \(\omega_{i}\) i normale \(\mathbf{n}\).
\end{itemize}

\clearpage

% Slika jednadžbe reflektivnosti
\begin{figure}[htb]
\[  L_{o}(\mathbf{x}, \omega_{o}, \lambda, t) = \int_{\Omega}{ f_{r}(\mathbf{x}, \omega_{i}, \omega_{o}, \lambda, t) L_{i}(\mathbf{x}, \omega_{i}, \lambda, t)(\omega_{i} \cdot \mathbf{n}) } \mathrm{d}\omega_{i} \]
\caption{Jednadžba reflektivnosti}
\label{fig:reflectance-equation}
\end{figure}

Izrazom na slici \ref{fig:reflectance-equation} je prikazan specijalizirani oblik jednadžbe ostvarivanja prikaza nazvan jednadžba reflektivnosti (engl. reflectance equation).
Jednadžba vrijedi za površine koje ne emitiraju svjetlost, to jest, za površine čija je emitirana sjajnost \(L_{e}\) jednaka nuli.
Unutar te jednadžbe jedina nepoznanica u ovom trenutku nam je funkcija distribucije dvosmjerne refleksije \(f_{r}\) - funkcija koja skalira ulaznu sjajnost na temelju svojstava materijala površine.
Naglasak na nju će biti u idućem poglavlju.
