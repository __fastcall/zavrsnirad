% Poglavlje "Osvjetljavanje temeljeno na slici"
\chapter{Osvjetljavanje temeljeno na slici}

% Potpoglavlje "Uzorkovanje kubne teksture"
\section{Uzorkovanje kubne teksture}

U programu sjenčara fragmenata često želimo realizirati zrcaljenje okoliša na objekt.
Postoji nekoliko načina da to ostvarimo.
Najjednostavniji način bi bio korištenje kubne teksture (engl. cubemap).
Primjer kubne teksture je prikazan u nastavku na slici \ref{fig:cubemap-example}.

% Slika primjera kubne teksture
\begin{figure}[htb]
\centering
\includegraphics[width=10cm]{figure/cubemap.png}
\caption{Primjer kubne teksture}
\label{fig:cubemap-example}
\end{figure}

Kubna tekstura se sastoji od 6 sastavnih tekstura, od kojih svaka odgovara jednoj od 6 ploha kocke.
Za svaki fragment objekta na kojemu želimo ostvariti zrcaljenje ćemo koristiti njegovu normalu u toj točki za izračun boje kubne teksture koja bi bila zrcaljena upravo u tom fragmentu.
Programski jezik GLSL ima ugrađenu metodu \texttt{texture} koja ima varijantu za parametar kubne teksture \texttt{samplerCube} i omogućuje nam upravo to bez potrebe za dodatnim kodom.

\clearpage

Na slici \ref{fig:sphere-utah-teapot-envmap} možemo vidjeti rezultat zrcaljenja kubne teksture okoliša na objektu kugle i čajnika.

% Slika primjera okoliša zrcaljenog na kugli i čajniku
\begin{figure}[htb]
\centering
\includegraphics[width=6.75cm]{figure/sphere-env.png}
\includegraphics[width=7.25cm]{figure/teapot-env.png}
\caption{Primjer okoliša zrcaljenog na kugli i čajniku}
\label{fig:sphere-utah-teapot-envmap}
\end{figure}

% Potpoglavlje "Ravnopravna projekcija"
\section{Ravnopravna projekcija}

Ravnopravna projekcija (engl. equirectangular projection) je jednostavni oblik projekcije kugle na ravnu plohu.
U implementaciji, teksture u toj projekciji su korištene za zapis teksture okoliša iz razloga što su jednostavnije za prijenos.
Za razliku od kubnih tekstura koje zahtjevaju 6 različitih tekstura za jedan okoliš, one zahtjevaju samo jednu.
Primjer teksture okoliša u ravnopravnoj projekciji je prikazan na slici \ref{fig:equirectangular-example}.

% Slika primjera okoliša prikazanog ravnopravnom projekcijom
\begin{figure}[htb]
\centering
\includegraphics[width=14cm]{figure/equirectangular.png}
\caption{Primjer okoliša prikazanog ravnopravnom projekcijom}
\label{fig:equirectangular-example}
\end{figure}

Teksture u ravnopravnoj projekciji bi željeli koristiti na isti način na koji koristimo i kubne teksture, a to je uzorkovanjem pomoću normale fragmenta.
Nažalost, GLSL nam ne pruža ovu funkcionalnost izravno te ćemo morati napisati vlastitu funkciju koja pretvara koordinate smjera normale u 2D koordinate teksture.
Navedena funkcija je prikazana na slici \ref{fig:equirectangular-glsl-sampler}.
Bitno za napomenuti jest da je ovaj pristup sporiji za razliku od izravnog korištenja kubne teksture, stoga je preporučeno da se statičke teksture u ravnopravnoj projekciji prije prvog korištenja pretvore u kubne teksture.

% Slika funkcije u GLSL-u za uzorkovanje teksture u ravnopravnoj projekciji
\begin{figure}[htb]
\lstset{language=GLSL, tabsize=2}
\begin{lstlisting}[basicstyle=\small]
#define PI 3.14159265358979323846
vec2 SampleEquirectangular(vec3 v) {
    vec2 uv = vec2(atan(v.z, v.x), asin(v.y));
    uv *= vec2(0.5 / PI, 1.0 / PI);
    uv += 0.5;
    return uv;
}
\end{lstlisting}
\caption{Funkcija u GLSL-u za uzorkovanje teksture u ravnopravnoj projekciji [\citenum{learnopengl}]}
\label{fig:equirectangular-glsl-sampler}
\end{figure}

\bigskip

% Potpoglavlje "Difuzna obasjanost"
\section{Difuzna obasjanost}

Prisjetimo se jednadžbe reflektivnosti i Cook-Torranceove aproksimacije funkcije distribucije dvosmjerne refleksije.
S obzirom da model definira funkciju kao zbroj međusobno neovisne difuzne i spekularne komponente pomnožene s vlastitim omjerom utjecaja, uvrštavanjem iste u jednadžbu reflektivnosti možemo rastaviti integralni izraz u dva podintegrala.
Izraz za difuzni podintegral je prikazan u nastavku na slici \ref{fig:reflectance-equation-diffuse}.

\bigskip
\bigskip

% Slika difuznog dijela jednadžbe reflektivnosti za Cook-Torranceov model
\begin{figure}[htb]
\[  L_{d}(\mathbf{x}, \omega_{o}, \lambda, t) = k_{d} \frac{c}{\pi} \int_{\Omega}{ L_{i}(\mathbf{x}, \omega_{i}, \lambda, t)(\omega_{i} \cdot \mathbf{n}) } \mathrm{d}\omega_{i} \]
\caption{Difuzni dio jednadžbe reflektivnosti za Cook-Torranceov model}
\label{fig:reflectance-equation-diffuse}
\end{figure}

\clearpage

Primjetimo da za svaki fragment integral difuzne komponente ovisi samo o ulaznom smjeru svjetlosti \(\omega_{i}\).
To znači da možemo koristiti taj izraz da unaprijed izračunamo kubnu mapu integralnog dijela za neku sliku okoliša koju bismo koristili za računanje difuzne komponente.
Prvo što moramo napraviti jest redefinirati integral pomoću stvarnih varijabli s kojima zapravo možemo računati.

\bigskip

% Slika integriranja po svakoj ulaznoj zraki svjetlosti
\begin{figure}[htb]
\centering
\includegraphics[width=9.25cm]{figure/diff_comp_int.png}
\caption{Integriranje po svakoj ulaznoj zraki svjetlosti}
\label{fig:diff-comp-int}
\end{figure}

\bigskip

Za obilazak čitavog prostora \(\Omega\), kao što je prikazano na slici \ref{fig:diff-comp-int}, možemo se kretati po površini ravnine od kuta \(0\) do \(2 \pi\), integrirajući za svaku vrijednost kuta \(\theta\) ulazne zrake svjetlosti četvrtine kružnog isječka.
Samim time skalarni umnožak jediničnih vektora \((\omega_{i} \cdot \mathbf{n})\) možemo izraziti pomoću \(\cos{(\frac{\pi}{2} - \theta)}\), ili jednostavnije, \(\sin{\theta}\).
Također, za izračun \(\omega_{i}\) pomoću kuteva možemo koristiti izraz \( (cos{\phi} \, cos{\theta}, sin{\theta}, -sin{\phi} \, cos{\theta}) \).
Na slici \ref{fig:reflectance-equation-diffuse-int} je prikazan konačan izraz.

\bigskip

% Slika redefiniranog difuznog dijela jednadžbe reflektivnosti za Cook-Torranceov model
\begin{figure}[htb]
\[ L_{d}(\mathbf{x}, \omega_{o}, \lambda, t) = k_{d} \frac{c}{\pi} \int_{\phi = 0}^{2 \pi}{ \int_{\theta = 0}^{\frac{\pi}{2}}{ L_{i}(\mathbf{x}, (cos{\phi} \, cos{\theta}, sin{\theta}, -sin{\phi} \, cos{\theta}), \lambda, t) \sin{\theta} } \,\mathrm{d}\theta } \,\mathrm{d}\phi \]
\caption{Redefiniran difuzni dio jednadžbe reflektivnosti za Cook-Torranceov model}
\label{fig:reflectance-equation-diffuse-int}
\end{figure}

\bigskip

Pretvorbom integrala u Riemannove sume možemo aproksimirati kubnu mapu okoliša.
Na primjeru kubne mape okoliša sa slike \ref{fig:cape-hill-original} ćemo dobiti kubnu mapu prikazanu na slici \ref{fig:cape-hill-irr}.

\clearpage

% Slika kubne mape okoliša
\begin{figure}[htb]
\centering
\includegraphics[width=10cm]{figure/cape_hill.png}
\caption{Kubna mapa okoliša}
\label{fig:cape-hill-original}
\end{figure}

\bigskip

% Slika kubne mape difuzne obasjanosti
\begin{figure}[htb]
\centering
\includegraphics[width=10cm]{figure/cape_hill_irr.png}
\caption{Kubna mapa difuzne obasjanosti}
\label{fig:cape-hill-irr}
\end{figure}

Vidimo da je kubna mapa difuzne obasjanosti dosta slična zamagljenoj ili zamućenoj slici.
S obzirom da ljudsko oko ne zamjećuje sitne detalje, mogli bismo koristiti alternativne metode manje složenosti za izračun kubne mape koju ćemo koristiti kao mapu difuzne obasjanosti.
Na slici \ref{fig:cape-hill-gauss} je prikazana kubna mapa okoliša na kojoj je provedeno Gaussovo zamućenje (engl. Gaussian blur).
Slika izgleda dosta slična onoj koja je izračunata aproksimativnim postupkom.
Alternativno tome, moguće je koristiti bilinearno uzorkovanje MIP mape niže rezolucije pozivom funkcije \texttt{textureLod}, kao što je prikazano na slici \ref{fig:cape-hill-mipmap}.

% Slika kubne mape dobivene Gaussovim zamućivanjem
\begin{figure}[H]
\centering
\includegraphics[width=10cm]{figure/cape_hill_gauss.png}
\caption{Kubna mapa dobivena Gaussovim zamućivanjem}
\label{fig:cape-hill-gauss}
\end{figure}

\bigskip

% Slika kubne mape dobivene bilinearnim uzorkovanjem
\begin{figure}[H]
\centering
\includegraphics[width=10cm]{figure/cape_hill_mipmap.png}
\caption{Kubna mapa dobivena bilinearnim uzorkovanjem}
\label{fig:cape-hill-mipmap}
\end{figure}

% Potpoglavlje "Spekularno osvjetljavanje"
\section{Spekularno osvjetljavanje}

Preostalo je još razriješiti spekularni dio jednadžbe reflektivnosti.
Nažalost, za razliku od difuznog dijela, ne možemo izdvojiti spekularnu komponentu izvan integrala jer njeni članovi ovise o više parametara osim samog ulaznog smjera svjetlosti \(\omega_{i}\).
Na slici \ref{fig:reflectance-equation-specular} je prikazan spekularni dio jednadžbe reflektivnosti.

\clearpage

% Slika spekularnog dijela jednadžbe reflektivnosti za Cook-Torranceov model
\begin{figure}[htb]
\[  L_{s}(\mathbf{x}, \omega_{o}, \lambda, t) = k_{s} \int_{\Omega}{ \frac{D F G}{4 (\omega_{o} \cdot \mathbf{n})} L_{i}(\mathbf{x}, \omega_{i}, \lambda, t) } \mathrm{d}\omega_{i} \]
\caption{Spekularni dio jednadžbe reflektivnosti za Cook-Torranceov model}
\label{fig:reflectance-equation-specular}
\end{figure}

\bigskip

Pogoni kao što su Unreal Engine to razrješavaju tako da rastavljaju umnožak unutar integrala u konvoluciju dva integrala.
Na slici \ref{fig:reflectance-equation-specular-split} vidimo razdvojeni izraz.
Integralni dio sa ulaznom svjetlosti \(L_{i}\) se rješava izračunom kubne mape slične onoj za difuznu obasjanost, no ovaj put trebamo uključiti i faktor gruboće.
Za veći faktor gruboće vektori smjera su raspršeniji rezultirajući zamućenijim rezultatom.
Koristeći funkciju distribucije normala s pretpostavkom da je vektor pogleda \(\hat{v}\) jednak vektoru smjera izlazne svjetlosti \(\omega_{o}\) trebamo generirati nekoliko razina kubnih mapa, te one sa većom gruboćom možemo spremiti, umjesto u zasebnu teksturu, u razinu MIP mape s manjom rezolucijom.
Na taj način, uzorkovanje trilinearne teksture možemo obavljati \texttt{textureLod} naredbom parametrizirajući razinu detalja (engl. level of detail) sa skaliranim faktorom gruboće.
Naravno, moguće je aproksimirati isto i na jednostavniji način koristeći automatski generirane razine MIP mape kubne teksture okoliša, što će rezultirati manje kvalitetnim rezultatom.

\bigskip

% Slika razdvojenog spekularnog dijela jednadžbe reflektivnosti za Cook-Torranceov model
\begin{figure}[htb]
\[  L_{s}(\mathbf{x}, \omega_{o}, \lambda, t) = k_{s} \int_{\Omega}{ \frac{D F G}{4 (\omega_{o} \cdot \mathbf{n})} } \mathrm{d}\omega_{i}  *  \int_{\Omega}{ L_{i}(\mathbf{x}, \omega_{i}, \lambda, t) } \mathrm{d}\omega_{i} \]
\caption{Razdvojeni spekularni dio jednadžbe reflektivnosti za Cook-Torranceov model}
\label{fig:reflectance-equation-specular-split}
\end{figure}

\bigskip

Dio integrala koji sadrži funkcija distribucije dvosmjerne refleksije se aproksimira principom razdvojenih suma (engl. split-sum approximation).
Princip uključuje razdvajanje integrala uvrštavanjem Fresnelovog koeficijenta na sumu dva integrala.
U sljedećem koraku je potrebno izračunati teksturu pregledne tablice (eng. look-up table, LUT) kojoj je na osi \(u\) postavljen faktor gruboće, a na osi \(v\) postavljen kosinus kuta između normale površine \(\mathrm{n}\) i vektora pogleda \(\hat{v}\).
Postupak je sličan prethodnom, a rezultantna tekstura bi trebala izgledati kao u nastavku na slici \ref{fig:splitsum}.

\bigskip

% Slika teksture pregledne tablice
\begin{figure}[H]
\centering
\includegraphics[width=6cm]{figure/splitsum.png}
\caption{Tekstura pregledne tablice}
\label{fig:splitsum}
\end{figure}
